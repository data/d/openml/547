# OpenML dataset: no2

https://www.openml.org/d/547

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Magne Aldrin (magne.aldrin@nr.no)  
**Source**: [StatLib](http://lib.stat.cmu.edu/datasets/) - 2004  
**Please cite**:   

The data are a subsample of 500 observations from a data set that originate in a study where air pollution at a road is
related to traffic volume and meteorological variables, collected by the Norwegian Public Roads Administration. The response variable (column 1) consist of hourly values of the logarithm of the concentration of NO2 (particles), measured at Alnabru in Oslo, Norway, between October 2001 and August 2003. 

The predictor variables (columns 2 to 8) are the logarithm of the number of cars per hour, temperature $$2$$ meter above ground (degree C), wind speed (meters/second), the temperature difference between $$25$$ and $$2$$ meters above ground (degree C), wind direction (degrees between 0 and 360), hour of day and day number from October 1. 2001.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/547) of an [OpenML dataset](https://www.openml.org/d/547). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/547/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/547/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/547/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

